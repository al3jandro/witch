import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  constructor(private api: ApiService) { }

  items$: Observable<any[]>;
  ngOnInit() {
    this.items$ = this.api.getJson('db');
  }

}
