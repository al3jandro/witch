import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuComponent } from './menu/menu.component';
import { ProgressDiaComponent } from './progress-dia/progress-dia.component';
import { MomentModule } from 'ngx-moment';
import { FiltroPipe } from '../pipes/filtro.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    ProgressDiaComponent,
    FiltroPipe
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    ProgressDiaComponent,
    FiltroPipe
  ],
  imports: [
    MomentModule,
    RouterModule,
    IonicModule,
    CommonModule
  ]
})
export class SharedModule { }
