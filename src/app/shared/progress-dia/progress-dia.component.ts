import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-progress-dia',
  templateUrl: './progress-dia.component.html',
  styleUrls: ['./progress-dia.component.scss'],
})
export class ProgressDiaComponent implements OnInit {

  @Input() start: Date;
  @Input() end: Date;
  today: number = Date.now();
  diff: any = [];
  constructor() { }

  ngOnInit() {
    const total = moment(moment(this.end).add(3, 'hour')).diff(moment(moment(this.start).add(3, 'hour')), 'days');
    const atual = moment(moment(this.end).add(3, 'hour')).diff(moment(this.today), 'days');
    this.diff = (total - atual) / total;
  }

}
