import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ApiService } from './services/api.service';
import { Observable } from 'rxjs';
import { ToastService } from './services/toast.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  items$: Observable<any[]>;
  exits: any = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private api: ApiService,
    private toast: ToastService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.items$ = this.api.getJson(`db`);
      this.getExits();
    });
  }

  logout() {
    this.api.signOut().subscribe(
      data => {
        this.api.removeLocalstorage('access');
        this.toast.Toast('Obrigado por usar nossa APP');
        return this.router.navigate(['/acesso']);
      }, err => console.log(err)
    );
  }

  getExits() {
    this.api.getCooperadas().subscribe(data => {
      // console.log(data);
    }, error => {
      console.log(error);
    });
  }
}
