import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private toast: ToastController
  ) { }

  async Toast(message: string, color: string = 'light', position: any= 'bottom') {
    const t = await this.toast.create({
      message,
      duration: 1500,
      color,
      position
    });
    return t.present();
  }
}
