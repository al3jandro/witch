export interface Cooperadas {
    id: number;
    name: string;
    slug: string;
    block: string;
    action: boolean;
    ordem: number;
    c_at: string;
}


export interface Component {
    id: number;
    lp: string;
    name: string;
    bloco: string;
    selector: string;
    parameters: string;
    status: number;
    c_at: string;
}
