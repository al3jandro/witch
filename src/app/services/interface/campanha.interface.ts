export interface Campanha {
    id: number;
    codigo: number;
    nome: string;
    description: string;
    total_produtos: number;
    load_produtos: number;
    vigencia_inicio: string;
    vigencia_fin: string;
    status: number;
    c_at: string;
}
