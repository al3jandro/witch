import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Produtos, Precos } from './interface/product.interface';
import { Campanha } from './interface/campanha.interface';
import { Cooperadas, Component } from './interface/all.interface';

const url: string = environment.mkt.url;
const tok: string = `access_token=${environment.mkt.tok}`;

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  getJson(db: string): Observable<any[]> {
    return this.http.get<any[]>(`./assets/${ db }.json`).pipe(tap(data => data));
  }

  private Query<T>( query: string ) {
    return this.http.get<T>(`${ url }/${ query }`);
  }


  getCampanhas(): Observable<Campanha[]> {
    const sql = `Campanhas?filter[where][status]=1&filter[order]=vigencia_fin%20ASC`;
    return this.Query<Campanha[]>(sql);
  }

  getCampanhaId(id: number): Observable<Campanha[]>  {
    const sql = `Campanhas/${ id }?${ tok }`;
    return this.Query<Campanha[]>(sql);
  }

  getFindCampanhaStatus( arg: string, val: string): Observable<Campanha[]> {
    const sql = `Campanhas/filter[where][${ arg }]=${ val }&filter[where][status]=1&${ tok }`;
    return this.Query<Campanha[]>(sql);
  }

  // Search Produtos
  getSearch(name: string): Observable<Produtos[]> {
    const sql = `Produtos/filter[where][dsc_produto][like]=%25${ name }%25&${ tok }`;
    return this.Query<Produtos[]>(sql);
  }

  // Produtos X Campanha
  getFindProductXCampanha(id: number): Observable<Produtos[]> {
    const sql = `Produtos?filter[where][campanhaId]=${ id }&${ tok }`;
    return this.Query<Produtos[]>(sql);
  }

  getAllProduct(): Observable<Produtos[]> {
    const sql = `Produtos?${ tok }`;
    return this.Query<Produtos[]>(sql);
  }

  /**
   * Get Produto ID
   * @param id Id do Produto
   */
  getProductId(id: number): Observable<Produtos[]> {
    const sql = `Produtos/${ id }&${ tok }`;
    return this.Query<Produtos[]>(sql);
  }

  getPrecosHost(host: number): Observable<Precos[]> {
    const sql = `Precos?filter[where][host_id]=${ host }&${ tok }`;
    return this.Query<Precos[]>(sql);
  }

  getPrecosId(id: number): Observable<Precos[]>  {
    const sql = `Precos/${ id }?${ tok }`;
    return this.Query<Precos[]>(sql);
  }

  getCooperadas(): Observable<Cooperadas[]> {
    const sql = `Cooperadas/groupCooperada?${ tok }`;
    return this.Query<Cooperadas[]>(sql);
  }

  getCooperada(slug: string) {
    const sql = `Cooperadas?filter[where][slug]=${ slug }&filter[order]=ordem%20ASC&${ tok }`;
    return this.Query<Cooperadas[]>(sql);
  }

  /** Builder */
  getComponent(slug: string): Observable<Component[]> {
    const sql = `Components?filter[where][lp]=${ slug }&${ tok }`;
    return this.Query<Component[]>(sql);
  }

  // Update
  update(table: string, id: number, data: any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http
    .put<any[]>(`${ url }/${ table }/${ id }`, data, {headers});
  }

  // Delete
  delete(table: string, id: string): Observable<any[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8'});
    return this.http.delete<any[]>(`${ url }/${ table }/${ id }`, { headers });
  }

  /**
   * Sign-In
   */
  signIn(data: any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post(`${ url }/Customers/login`, data, { headers });
  }

  /**
   * Sign-Out
   */
  signOut() {
    const accessToken = this.getAccessToken();
    return this.http.post(`${ url }/Customers/logout?access_token=${ accessToken }`, { });
  }

  /**
   * LocalStorage
   */
  isLoggedIn() {
    return localStorage.getItem('access') ? true : false;
  }

  getAccessToken() {
    return JSON.parse(localStorage.getItem('access')).id;
  }

  getAccessTokenUserId() {
    return JSON.parse(localStorage.getItem('access')).userId;
  }

  setLocalstorage(key: string, data: any) {
    return localStorage.setItem(key, JSON.stringify(data));
  }

  removeLocalstorage(key: string) {
    return localStorage.removeItem(key);
  }
}
