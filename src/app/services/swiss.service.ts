import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SwissService {

  constructor(
    private toast: ToastController
  ) { }

  async Toast(message: string, color: string = 'dark') {
    const toast = await this.toast.create({
      message,
      duration: 1500,
      color
    });
    return toast.present();
  }

}
