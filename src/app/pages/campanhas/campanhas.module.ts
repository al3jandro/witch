import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CampanhasPageRoutingModule } from './campanhas-routing.module';
import { CampanhasPage } from './campanhas.page';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    CampanhasPageRoutingModule
  ],
  declarations: [CampanhasPage],
  providers: [DatePipe]
})
export class CampanhasPageModule {}
