import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampanhasPage } from './campanhas.page';

const routes: Routes = [
  {
    path: '',
    component: CampanhasPage,
  },
  {
    path: 'edit/:id',
    loadChildren: () => import('./edit-campanha/edit-campanha.module')
    .then( m => m.EditCampanhaPageModule)
  },
  {
    path: 'view-produtos',
    loadChildren: () => import('./view-produtos/view-produtos.module').then( m => m.ViewProdutosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampanhasPageRoutingModule {}
