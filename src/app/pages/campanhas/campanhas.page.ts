import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { SwissService } from '../../services/swiss.service';

@Component({
  selector: 'app-campanhas',
  templateUrl: './campanhas.page.html',
  styleUrls: ['./campanhas.page.scss'],
})
export class CampanhasPage implements OnInit {

  items$: Observable<any[]>;
  constructor(
    private api: ApiService,
    private actionSheet: ActionSheetController,
    private alert: AlertController,
    private swiss: SwissService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getCampanha();
  }

  getCampanha() {
    this.items$ = this.api.getCampanhas();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getCampanha();
      event.target.complete();
    }, 2000);
  }

  async presentActionSheet(id: number) {
    const actionSheet = await this.actionSheet.create({
      mode: 'md',
      buttons: [
        {
          text: 'Ver Produtos',
          icon: 'eye',
          handler: () => {
            this.next('view-produtos', id);
          }
        },
        {
          text: 'Editar Campanha',
          icon: 'done-all',
          handler: () => {
            this.next('edit', id);
          }
        },
        {
          text: 'Apagar Campanha',
          icon: 'trash',
          handler: () => {
            this.delete(id);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async delete(id: any) {
    const alert = await this.alert.create({
      header: 'Info!',
      message: `Vaí apagar a Campanha?`,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancelar: blah');
          }
        },
        {
          text: 'Apagar',
          handler: () => {
            this.api.delete('Campanhas', id).subscribe(data => {
              this.swiss.Toast('Se apago a campanha!');
              this.getCampanha();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  next(parameters: string, id: number) {
    this.router.navigate(['campanhas/', parameters, id]);
  }
}
