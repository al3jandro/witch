import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-edit-campanha',
  templateUrl: './edit-campanha.page.html',
  styleUrls: ['./edit-campanha.page.scss'],
})
export class EditCampanhaPage implements OnInit {

  public id: any = [];
  item: any = [];

  constructor(
    private api: ApiService,
    private act: ActivatedRoute,
    private toast: ToastController,
    private router: Router
  ) {
    this.id = this.act.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getId(this.id);
  }

  onSubmit() {
    if (!this.item) {
      return;
    }
    this.api.update('Campanhas', this.item.id, this.item).subscribe(
      data => {
        this.showToast();
        this.router.navigate(['/campanhas']);
      }
    );
  }

  getId(id: number) {
    this.api.getCampanhaId(id).subscribe(
      data => this.item = data,
      err => console.log(err)
    );
  }

  async showToast() {
    const t = await this.toast.create({
      message: 'Campanha foi atualizada.',
      position: 'top',
      duration: 1500
    });
    t.present();
  }
}
