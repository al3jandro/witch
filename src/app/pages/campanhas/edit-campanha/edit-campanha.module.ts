import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCampanhaPageRoutingModule } from './edit-campanha-routing.module';

import { EditCampanhaPage } from './edit-campanha.page';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    EditCampanhaPageRoutingModule
  ],
  declarations: [EditCampanhaPage]
})
export class EditCampanhaPageModule {}
