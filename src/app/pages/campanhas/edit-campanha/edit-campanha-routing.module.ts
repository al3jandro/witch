import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCampanhaPage } from './edit-campanha.page';

const routes: Routes = [
  {
    path: '',
    component: EditCampanhaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCampanhaPageRoutingModule {}
