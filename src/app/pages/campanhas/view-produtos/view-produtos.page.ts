import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Produtos } from '../../../services/interface/product.interface';

@Component({
  selector: 'app-view-produtos',
  templateUrl: './view-produtos.page.html',
  styleUrls: ['./view-produtos.page.scss'],
})
export class ViewProdutosPage implements OnInit {

  public id: any = [];
  ocultar: boolean;
  items: Observable<Produtos[]>;
  search = '';
  constructor(
    private api: ApiService,
    private router: Router,
    private act: ActivatedRoute
  ) {
    this.id = this.act.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.ocultar = true;
    this.getProdutos(this.id);
  }

  doRefresh(event: any) {
    setTimeout(() => {
      this.getProdutos(this.id);
      event.target.complete();
    }, 2000);
  }

  buscar( event: any ) {
    this.search = event.detail.value;
  }


  getProdutos(id: number) {
    this.items = this.api.getFindProductXCampanha(id);
  }

  next(id: number) {
    this.router.navigate(['product', id]);
  }
}
