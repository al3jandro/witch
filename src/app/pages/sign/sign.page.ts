import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast.service';
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'app-sign',
  templateUrl: './sign.page.html',
  styleUrls: ['./sign.page.scss'],
})

export class SignPage implements OnInit {

  image = './assets/logo.png';
  fmLogin: FormGroup;

  constructor(
    private api: ApiService,
    private toast: ToastService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadForm();
  }

  loadForm() {
    this.fmLogin = this.fb.group({
      email: [[Validators.required, Validators.email]],
      password: [Validators.required]
    });
  }

  onLogin() {
    console.log(this.fmLogin.value);
    if (!this.fmLogin.valid) {
      return;
    }
    this.api.signIn(this.fmLogin.value).subscribe(
      data => {
        console.log(data);
        this.api.setLocalstorage('access', data);
        this.toast.Toast('Bem-vindo ao Sistema');
        this.router.navigate(['']);
      }, err => {
        this.toast.Toast('E-mail ou senha errada');
        this.fmLogin.reset();
      }
    );
  }
}
