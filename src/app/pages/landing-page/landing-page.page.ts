import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.page.html',
  styleUrls: ['./landing-page.page.scss'],
})
export class LandingPagePage implements OnInit {

  items$: Observable<any[]>;
  ocultar: boolean;

  constructor(
    private api: ApiService,
    private router: Router,
    private actionSheet: ActionSheetController
  ) { }

  ngOnInit() {
    this.ocultar = true;
    setTimeout(() => {
      this.getLP();
      this.ocultar = false;
    }, 1500);
  }

  getLP() {
    this.items$ = this.api.getCooperadas();
  }

  doRefresh(e: any) {
    setTimeout(() => {
      this.getLP();
      e.target.complete();
    }, 1500);
  }

  async show(slug: string) {
    const action = await this.actionSheet.create({
      mode: 'md',
      buttons: [
      {
        text: 'Editar Texto',
        icon: 'create',
        handler: () => {
          this.router.navigate(['/landing-page/alterar/', slug]);
        }
      }, {
        text: 'Blocos',
        icon: 'browsers',
        handler: () => {
          this.router.navigate(['/landing-page/activate/', slug]);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await action.present();
  }

}
