import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../../services/toast.service';

@Component({
  selector: 'app-alterar',
  templateUrl: './alterar.page.html',
  styleUrls: ['./alterar.page.scss'],
})
export class AlterarPage implements OnInit {

  slug: any = [];
  items: any = [];

  constructor(
    private act: ActivatedRoute,
    private api: ApiService,
    private toast: ToastService
  ) {
    this.slug = this.act.snapshot.paramMap.get('slug');
  }

  ngOnInit() {
    this.getAll(this.slug);
  }

  getAll(slug: string) {
    this.api.getComponent(slug).subscribe(
      data => {
        this.items = data;
      },
      err => console.log(err)
    );
  }

  update(item: any) {
    this.api.update('Components', item.id, item).subscribe(
      data => {
        this.toast.Toast(
          'Show! seu componente foi atualizado',
          'light',
          'top'
        );
        this.ngOnInit();
      }
    );
  }
}
