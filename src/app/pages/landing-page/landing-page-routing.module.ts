import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingPagePage } from './landing-page.page';

const routes: Routes = [
  {
    path: '',
    component: LandingPagePage
  },
  {
    path: 'activate/:slug',
    loadChildren: () => import('./activate/activate.module').then( m => m.ActivatePageModule)
  },
  {
    path: 'alterar/:slug',
    loadChildren: () => import('./alterar/alterar.module').then( m => m.AlterarPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LandingPagePageRoutingModule {}
