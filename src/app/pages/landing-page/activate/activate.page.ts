import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { SwissService } from 'src/app/services/swiss.service';
import { ActivatedRoute } from '@angular/router';
 
@Component({
  selector: 'app-activate',
  templateUrl: './activate.page.html',
  styleUrls: ['./activate.page.scss'],
})
export class ActivatePage implements OnInit {

  items$: Observable<any[]>;
  ocultar: boolean;
  slug: any = [];

  constructor(
    private act: ActivatedRoute,
    private api: ApiService,
    private swiss: SwissService
  ) {
    this.slug = this.act.snapshot.paramMap.get('slug');
  }

  ngOnInit() {
    this.ocultar = true;
    setTimeout(() => {
      this.getActivated(this.slug);
      this.ocultar = false;
    }, 1500);
  }

  getActivated(slug: string) {
    this.items$ = this.api.getCooperada(slug);
  }

  selectChange(c) {
    console.log(c);
    const rows = {
      action: c.action,
      block: c.block,
      name: c.name,
      ordem: c.ordem,
      slug: c.slug
    };
    if(c.action === true) {
      this.swiss.Toast(`Ativando o bloco ${c.block}`, 'primary');
    } else {
      this.swiss.Toast(`Desativando o bloco ${c.block}`, 'danger');
    }
    // this.api.update('Cooperadas', c.id, rows).subscribe(data => {});
  }
}
