import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPrecoPageRoutingModule } from './edit-preco-routing.module';

import { EditPrecoPage } from './edit-preco.page';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    EditPrecoPageRoutingModule
  ],
  declarations: [EditPrecoPage]
})
export class EditPrecoPageModule {}
