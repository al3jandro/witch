import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-edit-preco',
  templateUrl: './edit-preco.page.html',
  styleUrls: ['./edit-preco.page.scss'],
})
export class EditPrecoPage implements OnInit {

  public slug: any = [];
  public id: any = [];
  item: any = [];
  product: any = [];

  constructor(
    private api: ApiService,
    private act: ActivatedRoute,
    private toast: ToastController,
    private router: Router
  ) {
    this.id = this.act.snapshot.paramMap.get('id');
    this.slug = this.act.snapshot.paramMap.get('slug');
  }

  ngOnInit() {
    this.getId(this.slug);
    this.getProduct(this.id);
  }

  getId(slug: number) {
    this.api.getPrecosId(slug).subscribe(
      data => this.item = data,
      err => console.log(err)
    );
  }

  getProduct(id: number) {
    this.api.getProductId(id).subscribe(
      data => {
        this.product = data;
        console.log(data);
      },
      err => console.log(err)
    );
  }

  async showToast() {
    const t = await this.toast.create({
      message: 'O Preço foi atualizada.',
      position: 'top',
      duration: 1500
    });
    t.present();
  }

  update(item) {
    this.api.update('Precos', item.id, item).subscribe(
      data => {
        this.showToast();
        this.ngOnInit();
        this.router.navigate(['/product/', this.id]);
      }
    );
  }
}
