import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductPage } from './product.page';

const routes: Routes = [
  {
    path: ':id',
    component: ProductPage
  },
  {
    path: ':id/editar/:slug',
    loadChildren: () => import('./edit-preco/edit-preco.module').then( m => m.EditPrecoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductPageRoutingModule {}
