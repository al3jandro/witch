import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Precos } from '../../services/interface/product.interface';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  public id: any = [];
  item: any = [];
  preco: any = [];
  image: string = '';
  regao: string;

  constructor(
    private api: ApiService,
    private act: ActivatedRoute,
    private router: Router
  ) {
    this.id = this.act.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.product(this.id);
  }


  product(id: number) {
    this.api.getProductId(id).subscribe(
      data => {
        this.item = data;
        this.precos(this.item.cod_produto);
      },
      err => console.log(err)
    );
  }
  precos(host: number) {
    this.api.getPrecosHost(host).subscribe(
      preco => {
        this.preco = preco;
      },
      err => console.log(err)
    );
  }

  next(id: number, slug: number) {
    this.router.navigate([`/product/${id}/editar/${slug}`]);
  }
}
