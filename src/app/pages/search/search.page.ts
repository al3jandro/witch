import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  ocultar: boolean;
  items: Observable<any>;
  search = '';
  constructor(
    private api: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.ocultar = true;
    this.getProdutos();
  }

  doRefresh(event: any) {
    setTimeout(() => {
      this.getProdutos();
      event.target.complete();
    }, 2000);
  }

  buscar( event: any ) {
    this.search = event.detail.value;
  }


  getProdutos() {
    this.items = this.api.getAllProduct();
  }

  next(id: number) {
    this.router.navigate(['product', id]);
  }
}
